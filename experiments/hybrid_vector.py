import matplotlib.pyplot as plt
import numpy as np
import sklearn.cluster as cluster
import sklearn.manifold as manifold


def random_cross(centre: np.ndarray, standard_deviation: float, displacement: float, samples: int) -> np.ndarray:
    def create_deviations(long_axis: int) -> np.ndarray:
        deviations = np.array([standard_deviation for _ in range(centre.shape[0])])
        deviations[long_axis] *= displacement
        return np.diag(deviations)

    cross_a = np.random.default_rng().multivariate_normal(centre, create_deviations(0), samples // 2)
    cross_b = np.random.default_rng().multivariate_normal(centre, create_deviations(1), samples // 2)
    return np.concatenate((cross_a, cross_b), axis=0)


def main() -> None:
    samples_per_class = 500
    cross_dataset = np.concatenate((
        random_cross(np.array([0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), 0.01, 50.0, samples_per_class),
        random_cross(np.array([1.5, 1.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), 0.01, 50.0, samples_per_class),
        random_cross(np.array([4.5, 4.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), 0.02, 100.0, samples_per_class),
        random_cross(np.array([2.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), 0.02, 50.0, samples_per_class),
        random_cross(np.array([4.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), 0.02, 50.0, samples_per_class),
    ), axis=0)
    labels = np.concatenate((
        np.array([0 for _ in range(samples_per_class)]),
        np.array([1 for _ in range(samples_per_class)]),
        np.array([2 for _ in range(samples_per_class)]),
        np.array([3 for _ in range(samples_per_class)]),
        np.array([4 for _ in range(samples_per_class)]),
    ), axis=0)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(cross_dataset[:, 0], cross_dataset[:, 1], cross_dataset[:, 2], c=labels, cmap='Accent')
    ax.set_xlim(-5.0, 7.5)
    ax.set_ylim(-5.0, 7.5)
    ax.set_zlim(-5.0, 7.5)
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(111)
    raw_tsne = manifold.TSNE(n_components=2).fit_transform(cross_dataset)
    ax.scatter(raw_tsne[:, 0], raw_tsne[:, 1], c=labels, cmap='Accent')
    plt.show()

    clusters_per_class = 15
    cluster_centers = {
        i: cluster.KMeans(n_clusters=clusters_per_class).fit(
            cross_dataset[i * samples_per_class:(i + 1) * samples_per_class]
        ).cluster_centers_ for i in range(5)
    }

    class_embedded = np.concatenate(tuple(
        np.linalg.norm(cross_dataset[i * samples_per_class:(i + 1) * samples_per_class][:, None, :]
                       - cluster_centers[i][None, :, :], axis=-1)
        for i in range(5)
    ))

    perplexity = 120

    fig = plt.figure()
    ax = fig.add_subplot(111)
    class_tsne = manifold.TSNE(n_components=2, perplexity=perplexity).fit_transform(class_embedded)
    ax.scatter(class_tsne[:, 0], class_tsne[:, 1], c=labels, cmap='Accent')
    plt.show()

    global_clusters_no = 85
    global_cluster_centrers = cluster.KMeans(n_clusters=global_clusters_no).fit(cross_dataset).cluster_centers_
    global_embedded = np.linalg.norm(cross_dataset[:, None, :] - global_cluster_centrers[None, :, :], axis=-1)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    global_cluster_tsne = manifold.TSNE(n_components=2, perplexity=perplexity).fit_transform(global_embedded)
    ax.scatter(global_cluster_tsne[:, 0], global_cluster_tsne[:, 1], c=labels, cmap='Accent')
    plt.show()

    merged_embedding = np.concatenate((class_embedded, global_embedded), axis=1)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    merged_cluster_tsne = manifold.TSNE(n_components=2, perplexity=perplexity).fit_transform(merged_embedding)
    ax.scatter(merged_cluster_tsne[:, 0], merged_cluster_tsne[:, 1], c=labels, cmap='Accent')
    plt.show()


if __name__ == '__main__':
    main()
