import pandas as pd
from sklearn import svm
from sklearn.model_selection import train_test_split
from centroid_network.utils import read_dataset
from sklearn.metrics import accuracy_score

dataset_file_name = "mnist_0_100"

if __name__ == "__main__":
    # read_file = pd.read_csv("../datasets/{}.txt".format(dataset_file_name), delimiter=" ")
    # read_file.columns = ['x', 'y', 'label']
    # read_file.to_csv ("../datasets/{}.csv".format(dataset_file_name), index=None)

    dataset_train = read_dataset("../output/{}_random_labels_train.csv".format(dataset_file_name))
    dataset_test = read_dataset("../output/{}_random_labels_test.csv".format(dataset_file_name))

    # dataset_train, dataset_test = train_test_split(dataset, test_size=0.15)
    dataset_train_eval, _ = train_test_split(dataset_train, test_size=0.15)

    dataset_train = dataset_train.reset_index(drop=True)
    dataset_test = dataset_test.reset_index(drop=True)
    dataset_train_eval = dataset_train_eval.reset_index(drop=True)

    dataset_train_labels = dataset_train["label"]
    dataset_test_labels = dataset_test["label"]
    dataset_train_eval_labels = dataset_train_eval["label"]
    
    dataset_train = dataset_train.drop(labels=['label'], axis=1)
    dataset_test = dataset_test.drop(labels=['label'], axis=1)
    dataset_train_eval = dataset_train_eval.drop(labels=['label'], axis=1)
    
    svm = svm.SVC()
    dataset = svm.fit(dataset_train, dataset_train_labels)

    test_predictions = svm.predict(dataset_test)
    acc = accuracy_score(dataset_test_labels, test_predictions)
    print("Accuracy for test dataset: {}".format(acc))
    
    train_predictions = svm.predict(dataset_train_eval)
    acc = accuracy_score(dataset_train_eval_labels, train_predictions)
    print("Accuracy for train dataset: {}".format(acc))
