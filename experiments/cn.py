from sklearn.model_selection import train_test_split

from centroid_network.CentroidNetwork import CentroidNetwork
from centroid_network.utils import *


def logger_config():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[logging.FileHandler("../output/logs_fmnist_pca100.log"), logging.StreamHandler()],
    )


def experiment(dataset_file_name: str, local_centroids: int, global_centroids: int) -> None:
    # read dataset
    dataset = read_dataset("../datasets/{}.csv".format(dataset_file_name))

    dataset = normalize_input(dataset)

    # split and reindex the dataset
    dataset_train, dataset_test = train_test_split(dataset, test_size=0.15)
    dataset_train_eval, _ = train_test_split(dataset_train, test_size=0.15)

    dataset_train = dataset_train.reset_index(drop=True)

    dataset_test = dataset_test.reset_index(drop=True)
    dataset_test_labels = dataset_test['label']
    dataset_test = dataset_test.drop(labels=['label'], axis=1)

    dataset_train_eval = dataset_train_eval.reset_index(drop=True)
    dataset_train_eval_labels = dataset_train_eval['label']
    dataset_train_eval = dataset_train_eval.drop(labels=['label'], axis=1)

    CN = CentroidNetwork(global_centroids_count=global_centroids, num_processes=4)

    CN.append_layer(M=local_centroids)

    CN.multiprocess_train(X=dataset_train)

    # feed test data through network
    after_network_test = CN.multiprocess_feed_forward(X=dataset_test)

    score = evaluate(
        real_labels=dataset_test_labels, predicted_labels=after_network_test["label"]
    )
    logging.info("Accuracy for test dataset: {}".format(score))

    drop_to_csv(
        "../output/{}_{}_{}_test.csv".format(
            dataset_file_name, global_centroids, local_centroids
        ),
        after_network_test,
        dataset_test_labels,
    )

    # feed train data through network
    after_network_train = CN.multiprocess_feed_forward(X=dataset_train)
    score = evaluate(
        real_labels=dataset_train['label'], predicted_labels=after_network_train["label"]
    )
    logging.info("Accuracy for train dataset: {}".format(score))

    drop_to_csv(
        "../output/{}_{}_{}_train.csv".format(
            dataset_file_name, global_centroids, local_centroids
        ),
        after_network_train
    )

    after_network_train_eval = CN.multiprocess_feed_forward(X=dataset_train_eval)

    # feed eval train data through network
    score = evaluate(
        real_labels=dataset_train_eval_labels, predicted_labels=after_network_train_eval["label"],
    )
    logging.info("Accuracy for eval train dataset: {}".format(score))

    # save data set as all
    after_network = pd.concat([after_network_train, after_network_test])
    all_labels = pd.concat([dataset_train["label"], dataset_test_labels])

    drop_to_csv(
        "../output/{}_{}_{}_all.csv".format(
            dataset_file_name, global_centroids, local_centroids
        ),
        after_network
    )

    print(
        "Finished for {} dataset. Local count: {} and global count: {}.".format(
            dataset_file, local_centroids, global_centroids
        )
    )


if __name__ == "__main__":
    logger_config()

    dataset_files = ["mnist_70k_pca30"]
    local_centroids_values = [20, 50, 100, 200]
    global_centroids_values = [0, 50, 100, 200]

    for dataset_file in dataset_files:
        for local_centroid_value in local_centroids_values:
            for global_centroid_value in global_centroids_values:
                experiment(
                    dataset_file_name=dataset_file,
                    local_centroids=local_centroid_value,
                    global_centroids=global_centroid_value,
                )
