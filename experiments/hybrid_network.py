import keras
import matplotlib.pyplot as plt

from keras.layers import Dense
from keras.models import Sequential
from centroid_network.utils import *
from centroid_network.CentroidNetwork import CentroidNetwork
from sklearn.model_selection import train_test_split


def train_and_evaluate_keras_model(x_train, y_train, x_test, y_test, image_size: int = 0):
    # encode labels (needed for keras)

    num_classes = 10
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    print("First 5 training labels as one-hot encoded vectors:\n", y_train[:5])

    model = Sequential()

    model.add(Dense(units=32, activation='sigmoid', input_shape=(image_size,)))
    model.add(Dense(units=10, activation='softmax'))
    model.summary()

    model.compile(optimizer="adam", loss='categorical_crossentropy', metrics=['accuracy'])
    history = model.fit(x_train, y_train, batch_size=128, epochs=10, verbose=False, validation_split=.1)
    loss, accuracy = model.evaluate(x_test, y_test, verbose=False)

    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='best')
    plt.show()

    print(f'Test loss: {loss:.3}')
    print(f'Test accuracy: {accuracy:.3}')


def logger_config():
    import logging

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler("output/logs_mnist.log"),
            logging.StreamHandler()
        ]
    )


if __name__ == "__main__":
    # logger configuration
    logger_config()

    # read dataset
    dataset = read_dataset("../datasets/mnist.csv")
    dataset = normalize_input(dataset)

    # split the dataset on train/test and separate labels
    dataset_train, dataset_test = train_test_split(dataset, test_size=0.2, random_state=42)
    x_train, y_train = separate_labels_from_dataset(dataset_train)
    x_test, y_test = separate_labels_from_dataset(dataset_test)

    print("Training data shape: ", x_train.shape)
    print("Test data shape", x_test.shape)

    # centroid network training
    CN = CentroidNetwork()
    CN.append_layer(M=100)
    CN.multiprocess_train(X=dataset_train)
    after_network_train = CN.multiprocess_feed_forward(dataset_train)
    after_network_test = CN.multiprocess_feed_forward(dataset_test)

    # train and evaluate keras model using original data (no centroid network)
    train_and_evaluate_keras_model(x_train=x_train,
                                   y_train=y_train,
                                   x_test=x_test,
                                   y_test=y_test,
                                   image_size=784)

    # train and evaluate keras model using data feeded through centroid network
    x_train, y_train = separate_labels_from_dataset(after_network_train)
    x_test, y_test = separate_labels_from_dataset(after_network_test)

    train_and_evaluate_keras_model(x_train=x_train,
                                   y_train=y_train,
                                   x_test=x_test,
                                   y_test=y_test,
                                   image_size=100)
