import pandas as pd
from sklearn.decomposition import PCA

from centroid_network.utils import read_dataset

dataset_file_name = "fmnist"

if __name__ == "__main__":
	dataset = read_dataset("../datasets/{}.csv".format(dataset_file_name))

	labels = dataset['label']
	dataset = dataset.drop(labels=['label'], axis=1)

	pca = PCA(n_components=500)
	dataset = pca.fit_transform(dataset)
	dataset = pd.DataFrame(dataset)

	dataset['label'] = labels

	file_name = "../datasets/{}_pca500.csv".format(dataset_file_name)
	with open(file_name, 'a') as file:
		file.write(str(70000) + "\n")
		file.write(str(500) + "\n")
		dataset.to_csv(file_name, header=False, index=False)


