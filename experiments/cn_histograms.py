from centroid_network.utils import *
from centroid_network.CentroidNetwork import CentroidNetwork
from tqdm import tqdm
from sklearn.model_selection import train_test_split
from typing import List, Dict
from collections import OrderedDict

experiment_count = 0


def calculate_distances(data: Dict[Label, List]) -> Dict[Label, List[float]]:
    from scipy.spatial import distance

    distances = defaultdict(list)

    for label in tqdm(data):
        for i in range(0, len(data[label])):
            for j in range(i + 1, len(data[label])):
                point_i = data[label][i]
                point_j = data[label][j]

                distances[str(label)].append(distance.euclidean(
                    point_i.coordinates,
                    point_j.coordinates
                ))
    return distances


def plot_distances(
        distances: Dict[Label, List[float]],
        file_path: str = None,
        centroids: int = None) -> None:
    import matplotlib.pyplot as plt
    import seaborn as sns

    fig, axes = plt.subplots(nrows=5, ncols=2, figsize=(15, 15))
    fig.subplots_adjust(hspace=0.5)
    if centroids is not None:
        fig.suptitle('Distributions of embedded distances for M={}'.format(centroids))
    else:
        fig.suptitle('Distributions of original distances')

    for ax, label in zip(axes.flatten(), distances.keys()):
        sns.distplot(distances[label], ax=ax, bins=100)
        ax.set(title=label, xlabel='Distance')

    if file_path is not None:
        fig.savefig(file_path)

    plt.show()


def experiment(M: int = 10) -> None:
    global experiment_count

    CN = CentroidNetwork()
    CN.append_layer(M=M)
    CN.train(X=data_train, Y=labels_train)
    predicted_labels, embeddings = CN.predict(X=data_test, number_of_elements=3)

    if experiment_count == 0:
        original_distances = calculate_distances(OrderedDict(sorted(CN.input.items())))
        plot_distances(original_distances, "../pics/original_distances.png")

    embedded_distances = defaultdict(list)

    for i in range(0, len(embeddings)):
        embedded_distances[predicted_labels[i]].append(embeddings[i])

    embedded_distances = calculate_distances(OrderedDict(sorted(embedded_distances.items())))
    plot_distances(embedded_distances, "../pics/embedded_distances_M_{}.png".format(M), centroids=M)

    experiment_count += 1


if __name__ == "__main__":
    dataset = read_dataset("../datasets/mnist_21k_pca30.csv")

    dataset = normalize_input(dataset)

    dataset_train, dataset_test = train_test_split(dataset, test_size=0.3)

    data_train, labels_train = separate_labels_from_dataset(dataset_train)
    data_test, labels_test = separate_labels_from_dataset(dataset_test)

    M_values = [5, 10, 50, 100, 200]
    for M_value in M_values:
        experiment(M_value)
