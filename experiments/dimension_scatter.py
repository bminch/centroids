import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

dataframe = pd.read_csv('../output/mnist_21k_20.csv', skiprows=2)

columns = []
for i in range(0, dataframe.shape[1] - 1):
    columns.append("{}".format(i))
columns.append("label")
dataframe.columns = columns

dict_of_classes = dict(iter(dataframe.groupby('label')))
labels = dataframe['label']
dataframe = dataframe.drop(labels=['label'], axis=1)

unique = list(set(labels))

plt.figure(figsize=(18.44, 15.77))
sequence_of_colors = ["r", "orange", "y", "g", "b", "c", "k", "darkred", "teal", "indigo"]

j = 0
j_max = 2
k = 0
i = 0

keys = [2, 3]
for key in keys:
    df = dict_of_classes[key].values
    # plt.figure()
    for coords in df:
        k += 1
        if k == 1000:
            break
        coords = coords[:-1]
        plt.scatter(dataframe.columns, coords, c=sequence_of_colors[key])
    j += 1
    if j == j_max:
        break
    plt.savefig("../pics/coords.png".format(j))

plt.show()
