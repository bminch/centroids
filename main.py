from sklearn.model_selection import train_test_split
from centroid_network.CentroidNetwork import CentroidNetwork
from centroid_network.utils import *

global_centroids = 120
local_centroids = 10
dataset_file_name = "mnist"


def logger_config():
    import logging

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler("output/{}.log".format(dataset_file_name)),
            logging.StreamHandler(),
        ],
    )


if __name__ == "__main__":
    logger_config()

    # read dataset
    dataset = read_dataset("../dataset_viskit/{}.csv".format(dataset_file_name))

    # normalize input
    dataset = normalize_input(dataset)

    # random labels
    # dataset["label"] = np.random.randint(0, 10, dataset.shape[0])

    # split and reindex the dataset
    dataset_train, dataset_test = train_test_split(dataset, test_size=0.15)
    dataset_train_eval, _ = train_test_split(dataset_train, test_size=0.15)

    dataset_train = dataset_train.reset_index(drop=True)

    dataset_test = dataset_test.reset_index(drop=True)
    dataset_test_labels = dataset_test['label']
    dataset_test = dataset_test.drop(labels=['label'], axis=1)

    dataset_train_eval = dataset_train_eval.reset_index(drop=True)
    dataset_train_eval_labels = dataset_train_eval['label']
    dataset_train_eval = dataset_train_eval.drop(labels=['label'], axis=1)

    # create centroid network
    CN = CentroidNetwork(global_centroids_count=global_centroids, num_processes=4)

    # append layers with M centroids to the network
    CN.append_layer(M=local_centroids)
    # CN.append_layer(M=local_centroids_2)

    # train centroid network
    CN.multiprocess_train(X=dataset_train)

    # feed test data through network
    after_network_test = CN.multiprocess_feed_forward(X=dataset_test)

    score = evaluate(
        real_labels=dataset_test_labels, predicted_labels=after_network_test["label"]
    )
    logging.info("Accuracy for test dataset: {}".format(score))

    drop_to_csv(
        "output/{}_{}_{}_test.csv".format(
            dataset_file_name, global_centroids, local_centroids
        ),
        after_network_test,
        dataset_test_labels,
    )

    # feed train data through network
    after_network_train = CN.multiprocess_feed_forward(X=dataset_train)
    score = evaluate(
        real_labels=dataset_train['label'], predicted_labels=after_network_train["label"]
    )
    logging.info("Accuracy for train dataset: {}".format(score))

    drop_to_csv(
        "output/{}_{}_{}_train.csv".format(
            dataset_file_name, global_centroids, local_centroids
        ),
        after_network_train
    )

    after_network_train_eval = CN.multiprocess_feed_forward(X=dataset_train_eval)

    # feed eval train data through network
    score = evaluate(
        real_labels=dataset_train_eval_labels, predicted_labels=after_network_train_eval["label"],
    )
    logging.info("Accuracy for eval train dataset: {}".format(score))

    # save data set as all
    after_network = pd.concat([after_network_train, after_network_test])
    all_labels = pd.concat([dataset_train["label"], dataset_test_labels])

    drop_to_csv(
        "output/{}_{}_{}_all.csv".format(
            dataset_file_name, global_centroids, local_centroids
        ),
        after_network,
        all_labels
    )

    print("Finished.")
