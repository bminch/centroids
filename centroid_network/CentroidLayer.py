import logging
import multiprocessing
from collections import defaultdict

import numpy as np
import pandas as pd
from scipy.spatial import distance
from sklearn.cluster import KMeans
from sklearn.svm import SVC

from centroid_network.HDPoint import Label, Coordinates

lock = multiprocessing.Lock()


class CentroidLayer:
    """
    Representation of a centroid layer. It contains centroids calculated for each class
    during the train method.

    Attributes:
        local_centroids_count: Number of centroids, which should be calculated for each class.
        network_order: Determine which layer of the network this is.
    """

    def __init__(
        self,
        local_centroids_count: int = 1,
        network_order: int = 0,
        num_processes: int = 1,
    ):
        self.local_centroids_count = local_centroids_count

        if num_processes > 1:
            self.centroids = multiprocessing.Manager().dict()
        else:
            self.centroids = defaultdict()

        self.network_order = network_order
        self.temp_centroids = defaultdict(list)
        self.svm = SVC()
        self.logger = logging.getLogger("centroid_network_app")

    def fit_svm(self, X: pd.DataFrame) -> None:
        y = X["label"]
        X = X.drop(labels=["label"], axis=1)
        self.svm.fit(X, y)

    def train(self, X: pd.DataFrame):
        """
        Calculate centroids in this layer.
            Args:
                X: Training DataFrame (with label column).
        """
        Y = X["label"]
        X = X.drop(labels=["label"], axis=1)

        centroids = []
        for label in Y.unique():
            coordinates = X.loc[Y == label].values
            
            classifier = KMeans(
                n_clusters=self.local_centroids_count, random_state=0
            ).fit(coordinates)
            for center in classifier.cluster_centers_:
                centroids.append(center)

            lock.acquire()
            self.centroids[label] = centroids
            centroids = []
            lock.release()

        self.logger.info("Training for layer finished")

    def feed_forward(self, X: pd.DataFrame) -> [pd.DataFrame]:
        """
        Feed the provided data through this layer with no labels provided.
            Args:
                X: Input DataFrame.
            Returns:
                DataFrame with labels and embeddings obtained after the layer.
        """
        embeddings = pd.DataFrame()
        labels = pd.Series()
        Y = None

        if "label" in X.columns:
            Y = X['label'].to_numpy()
            X = X.drop(labels=["label"], axis=1)

        i = 0
        for _, row in X.iterrows():
            label = self.svm.predict([row.to_numpy()])[0]
            embedding = self.__feed_forward(row.values, label)
            embeddings = embeddings.append([embedding], ignore_index=True)

            if Y is None:
                labels = labels.append(pd.Series(label), ignore_index=True)

            i += 1

        if Y is None:
            embeddings["label"] = labels
        else:
            embeddings["label"] = Y

        return embeddings

    def __feed_forward(self, X: Coordinates, Y: Label = None) -> [Label, Coordinates]:
        """
        Feed the provided coordinates through this layer and embed it.
            Args:
                X: Coordinates of a data point.
            Returns:
                Label and coordinates of obtained embedding.
        """
        embedding = np.array([])

        for centroid in self.centroids[Y]:
            # dist = math.pow(np.sum(np.abs((X - centroid))**fraction), 1/fraction)
            dist = distance.minkowski(u=X, v=centroid, p=2)
            embedding = np.append(embedding, dist)

        return embedding

