import logging
import multiprocessing
import os
import numpy as np
import pandas as pd

from collections import defaultdict
from typing import Any
from sklearn import preprocessing

multiprocessing.set_start_method('spawn', True)


class ProcessHelper:
    def __init__(self, num_processes=4):
        self.num_processes = num_processes

    def map_reduce(self, function, df) -> pd.DataFrame:
        df_split = np.array_split(df, self.num_processes)
        pool = multiprocessing.Pool(processes=self.num_processes)
        results = pool.map(function, df_split)
        df = pd.concat(results)
        pool.close()
        pool.join()

        return df

    def map_reduce_train(self, function, df) -> None:
        unique_labels = df.label.unique()

        df_split = []
        for label in unique_labels:
            df_split.append(df.loc[df['label'] == label])

        pool = multiprocessing.Pool(processes=self.num_processes)
        pool.map(function, df_split)
        pool.close()
        pool.join()


def set_dataframe_columns(dataframe: pd.DataFrame) -> None:
    # set up dataset columns id
    columns = []
    for i in range(0, dataframe.shape[1] - 1):
        columns.append("col{}".format(i))
    columns.append("label")
    dataframe.columns = columns


def read_dataset(filepath: str) -> [pd.DataFrame]:
    data = pd.read_csv(filepath, skiprows=0, header=None)
    set_dataframe_columns(data)

    return data


def split_data_frame_by_class(x: pd.DataFrame, labels: pd.Series):
    dataframe_collection = defaultdict(pd.DataFrame)

    temp_labels = labels.unique()
    for label in temp_labels:
        mask = labels == label
        dataframe_collection[str(label)] = x[mask]

    return dataframe_collection


def normalize_input(data: pd.DataFrame) -> pd.DataFrame:
    labels = data["label"]
    data = data.drop(labels=["label"], axis=1)

    x = data.values
    min_max_scaled = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaled.fit_transform(x)

    data = pd.DataFrame(x_scaled)

    # add back copied labels and return
    return data.join(pd.DataFrame(labels))


def sigmoid(x, derivative=False):
    if derivative:
        return x * (1 - x)
    return 1 / (1 + np.exp(-x))


def evaluate(real_labels: pd.Series, predicted_labels: pd.Series) -> Any:
    logger = logging.getLogger("centroid_network_app")
    score = 0
    for i in range(0, real_labels.shape[0]):
        if real_labels.iloc[i] == predicted_labels.iloc[i]:
            score += 1
    return score / real_labels.shape[0]


def separate_labels_from_dataset(
        data_frame: pd.DataFrame,
) -> [pd.DataFrame, pd.DataFrame]:
    labels = data_frame["label"]
    data_frame = data_frame.drop(labels=["label"], axis=1)

    labels = labels.reset_index(drop=True)
    data_frame = data_frame.reset_index(drop=True)

    return data_frame, labels


def drop_to_csv(
        file_name: str, predictions: pd.DataFrame, labels: pd.Series = None
) -> None:
    logger = logging.getLogger("centroid_network_app")
    logger.info("Saving test dataset to file...")

    # create output dir if it doesn't exist
    if not os.path.exists('./output/'):
        os.makedirs('./output/')

    if labels is not None:
        predictions['label'] = labels

    with open(file_name, 'a') as file:
        predictions.to_csv(file, header=False, index=False)

    logger.info("Saved to csv file.")
