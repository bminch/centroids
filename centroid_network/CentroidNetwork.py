import logging
import time
import random

import numpy as np
import pandas as pd
from scipy.spatial import distance
from sklearn.cluster import KMeans
from sklearn.svm import SVC

from centroid_network.CentroidLayer import CentroidLayer
from centroid_network.utils import sigmoid, ProcessHelper, set_dataframe_columns


class CentroidNetwork:
    """
    Representation of a centroid network.
    It consist of Centroid Layers, which can be trained and then
    used to feed forward the data through network.

    Data are embedded as follows:
    For each datapoint in the dataset a distance (e.g. euclidean distance)
    from each centroid (from class) is calculated and added to output vector (embedding).

    As the results of predict method (output) we obtain a vector of distances
    (from data point to centroids) calculated for a specific class.
    Number of centroids calculated for each class in a layer is regulated
    with M parameter. Layer can be added by using append_layer method.

    Attributes:
        num_processes: Amount of processes, that will be used for training
        and feed forwarding.
        global_centroids_count: Number of centroids, which will be calculated on
        the whole dataset and then used in embedding process together with local centroids.
    """

    def __init__(self, global_centroids_count=0, num_processes=1):
        self.logger = logging.getLogger("centroid_network_app")
        self.num_processes = num_processes
        self.process_helper = ProcessHelper(num_processes)
        self.global_centroids_count = global_centroids_count
        self.global_centroids = []
        self.layers = []
        self.svm = SVC()

        self.logger.info(
            "{} global centroids will be calculated for this network.".format(
                global_centroids_count
            )
        )

    def append_layer(self, M: int = 10) -> None:
        """
        Add a new layer to the centroid network.
            Args:
                M: Number of local centroids calculated for each class during
                training process.
        """
        self.layers.append(CentroidLayer(M, len(self.layers), self.num_processes))
        self.logger.info(
            "Layer with {} centroids per class appended to network.".format(M)
        )

    def multiprocess_train(self, X: pd.DataFrame) -> None:
        """
        Trains (calculate centroids in each layer) the network with provided dataset and labels.
        For len(layers) > 1 it uses feed forward method to create dataset embedding based upon which
        centroids of the next layer will be calculated.
            Args:
                X: Input DataFrame containing both coordinates and labels.
        """
        self.logger.info("[Training] Start...")
        start = time.time()

        self.logger.info("[Training] Calculating local centroids")
        if len(self.layers) != 0:
            self.process_helper.map_reduce_train(self.train, X)

        if self.global_centroids_count != 0:
            self.logger.info("[Training] Calculating global centroids.")
            self.calculate_global_centroids()

        self.logger.info("[Training] Fitting SVM")

        if len(self.layers) > 0:
            self.layers[-1].fit_svm(X)
        else:
            y = X["label"]
            X = X.drop(labels=["label"], axis=1)
            self.svm.fit(X, y)

        self.logger.info("[Training] Finished. Time: {}".format(time.time() - start))

    def multiprocess_feed_forward(self, X: pd.DataFrame) -> [pd.DataFrame]:
        """
        Feed the provided data through the network (through all layers) with no labels provided.
            Args:
                X: Input dataset (pd.DataFrame).
           Returns:
                DataFrame containing predicted embeddings and labels.
        """
        self.logger.info("[Feeding] Start...")
        X = X.reset_index(drop=True)
        start = time.time()

        global_embedded = None
        if self.global_centroids_count != 0:
            self.logger.info("[Feeding] Using global centroids.")
            global_embedded = self.process_helper.map_reduce(
                self.feed_forward_global_centroids, X
            )

            if "label" in global_embedded.columns:
                global_embedded = global_embedded.drop(labels=["label"], axis=1)

        self.logger.info("[Feeding] Using local centroids.")

        local_embedded = []
        if len(self.layers) > 0:
            local_embedded = self.process_helper.map_reduce(
                self.feed_forward_local_centroids, X
            )

        if self.global_centroids_count != 0 and len(self.layers) > 0:
            result = pd.concat([global_embedded, local_embedded], axis=1)
        elif self.global_centroids_count != 0 and len(self.layers) == 0:
            result = global_embedded
        else:
            result = local_embedded

        set_dataframe_columns(result)
        self.logger.info("[Feeding] Finished. Time [s]: {}".format(time.time() - start))

        result = result.reset_index(drop=True)
        return result

    def train(self, X: pd.DataFrame) -> None:
        """
        Trains (calculate centroids in each layer) the network with provided dataset and labels.
        For len(layers) > 1 it uses feed forward method to create dataset embedding based upon which
        centroids of the next layer will be calculated.
            Args:
                X: Input DataFrame containing both coordinates and labels.
        """
        training_X = X

        # If there are more than just 1 layer - use temp variable with self.input value and:
        # 1) train the first layer with temp
        # 2) feed forward the data through trained layer and save embeddings to temp
        # 3) train the next layer
        if len(self.layers) > 1:
            for i in range(0, len(self.layers)):
                self.layers[i].train(training_X)
                if i < len(self.layers) - 1:
                    training_X = self.layers[i].feed_forward(training_X)

        else:
            self.layers[0].train(training_X)

    def feed_forward_global_centroids(self, X: pd.DataFrame) -> [pd.DataFrame]:
        """
        Feed the provided data through the network's global centroids with no labels provided.
            Args:
                X: Input dataset.
            Returns:
                DataFrame with embeddings and labels obtained after global centroids.
        """
        embeddings = pd.DataFrame()
        labels = None

        if "label" in X.columns:
            labels = X["label"]
            X = X.drop(labels=["label"], axis=1)

        after_centroids = np.array([])
        for _, row in X.iterrows():
            for centroid in self.global_centroids:
                # dist = math.pow(np.sum(np.abs((X - centroid))**fraction), 1/fraction)
                dist = distance.minkowski(u=row, v=centroid, p=2)
                after_centroids = np.append(after_centroids, sigmoid(dist))

            if len(self.layers) == 0:
                label = self.svm.predict([row.to_numpy()])[0]
                after_centroids = np.append(after_centroids, label)
            embeddings = embeddings.append([after_centroids], ignore_index=True)
            after_centroids = np.array([])

        if labels is not None and len(self.layers) > 0:
            embeddings["label"] = labels

        return embeddings

    def feed_forward_local_centroids(self, X: pd.DataFrame) -> [pd.DataFrame]:
        """
        Feed the provided data through the network's local centroids with no labels provided.
            Args:
                X: Input dataset.
            Returns:
                DataFrame with embeddings and labels obtained after local centroids.
        """
        data = X
        for i in range(0, len(self.layers)):
            data = self.layers[i].feed_forward(data)

        return data

    def calculate_global_centroids(self):
        if len(self.layers) > 0:
            centroids = list(self.layers[-1].centroids.values())
            flatten = [item for sublist in centroids for item in sublist]
            for i in range(0, self.global_centroids_count):
                self.global_centroids.append(random.choice(flatten))

