import unittest
import numpy as np

from pandas import DataFrame
from sklearn.cluster import KMeans
from centroid_network.CentroidLayer import CentroidLayer
from centroid_network.HDPoint import DataPoint
from sklearn.datasets import make_blobs
from collections import defaultdict


class TestLayer(unittest.TestCase):
    def setUp(self):
        X, y = make_blobs(n_samples=100, centers=3, n_features=2)

        self.df = DataFrame(dict(x=X[:, 0], y=X[:, 1], label=y))

        self.data = defaultdict(list)
        for i in range(0, len(X)):
            self.data[y[i]].append(DataPoint(coords=[X[i, 0], X[i, 1]]))

        self.layer = CentroidLayer()

    def test_train(self):
        self.layer.M = 3
        self.layer.train(self.data)

        k_means_centroids = []
        for label in self.data.keys():
            temp = self.df.loc[self.df['label'] == label]
            temp = temp.drop(labels=['label'], axis=1)
            k_means = KMeans(n_clusters=3, random_state=0).fit(temp)
            k_means_centroids.append(k_means.cluster_centers_)

        k_means_centroids = [item for sublist in k_means_centroids for item in sublist]

        layer_centroids_coords = self.layer.get_centroids_coordinates()
        layer_centroids_coords.sort(key=lambda x: x[1])
        k_means_centroids.sort(key=lambda x: x[1])

        self.assertIsNotNone(self.layer.centroids)
        self.assertIsNotNone(k_means_centroids)

        np.testing.assert_array_equal(layer_centroids_coords, k_means_centroids)
