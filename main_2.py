import numba
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from centroid_network.CentroidNetwork import CentroidNetwork
from centroid_network.utils import *
from scipy.spatial.distance import squareform, pdist
from sklearn.manifold import TSNE

global_centroids = 100
local_centroids = 100
local_centroids_2 = 0
dataset_file_name = "fmnist_pca100"
# dataset_file_name = "mnist_7k_pca30"


def logger_config():
    import logging

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler("output/{}.log".format(dataset_file_name)),
            logging.StreamHandler(),
        ],
    )


@numba.jit(nopython=True)
def knngain(d_hd, d_ld, labels):
    # Number of data points
    N = d_hd.shape[0]
    N_1 = N - 1
    k_hd = np.zeros(shape=N_1, dtype=np.int64)
    k_ld = np.zeros(shape=N_1, dtype=np.int64)
    # For each data point
    for i in range(N):
        c_i = labels[i]
        di_hd = d_hd[i, :].argsort(kind='mergesort')
        di_ld = d_ld[i, :].argsort(kind='mergesort')
        # Making sure that i is first in di_hd and di_ld
        for arr in [di_hd, di_ld]:
            for idj, j in enumerate(arr):
                if j == i:
                    idi = idj
                    break
            if idi != 0:
                arr[idi] = arr[0]
            arr = arr[1:]
        for k in range(N_1):
            if c_i == labels[di_hd[k]]:
                k_hd[k] += 1
            if c_i == labels[di_ld[k]]:
                k_ld[k] += 1
    # Computing the KNN gain
    gn = (k_ld.cumsum() - k_hd.cumsum()).astype(np.float64) / ((1.0 + np.arange(N_1)) * N)
    # Returning the KNN gain and its AUC
    return gn, eval_auc(gn)


@numba.jit(nopython=True)
def eval_auc(arr):
    i_all_k = 1.0 / (np.arange(arr.size) + 1.0)
    return np.float64(arr.dot(i_all_k)) / (i_all_k.sum())


@numba.jit(nopython=True)
def eval_rnx(Q):
    N_1 = Q.shape[0]
    N = N_1 + 1
    # Computing Q_NX
    qnxk = np.empty(shape=N_1, dtype=np.float64)
    acc_q = 0.0
    for K in range(N_1):
        acc_q += (Q[K, K] + np.sum(Q[K, :K]) + np.sum(Q[:K, K]))
        qnxk[K] = acc_q / ((K + 1) * N)
    # Computing R_NX
    arr_K = np.arange(N_1)[1:].astype(np.float64)
    rnxk = (N_1 * qnxk[:N_1 - 1] - arr_K) / (N_1 - arr_K)
    # Returning
    return rnxk


def eval_dr_quality(d_hd, d_ld):
    # Computing the co-ranking matrix of the embedding, and the R_{NX}(K) curve.
    rnxk = eval_rnx(Q=coranking(d_hd=d_hd, d_ld=d_ld))
    # Computing the AUC, and returning.
    return rnxk, eval_auc(rnxk)


def coranking(d_hd, d_ld):
    # Computing the permutations to sort the rows of the distance matrices in HDS and LDS.
    perm_hd = d_hd.argsort(axis=-1, kind='mergesort')
    perm_ld = d_ld.argsort(axis=-1, kind='mergesort')

    N = d_hd.shape[0]
    i = np.arange(N, dtype=np.int64)
    # Computing the ranks in the LDS
    R = np.empty(shape=(N, N), dtype=np.int64)
    for j in range(N):
        R[perm_ld[j, i], j] = i
    # Computing the co-ranking matrix
    Q = np.zeros(shape=(N, N), dtype=np.int64)
    for j in range(N):
        Q[i, R[perm_hd[j, i], j]] += 1
    # Returning
    return Q[1:, 1:]


def viz_qa(Ly, ymin=None, ymax=None, Lmarkers=None, Lcols=None, Lleg=None, Lls=None, Lmedw=None, Lsdots=None, lw=2,
           markevery=0.1, tit='', xlabel='', ylabel='', alpha_plot=0.9, alpha_leg=0.8, stit=25, sax=20, sleg=15, zleg=1,
           loc_leg='best', ncol_leg=1, lMticks=10, lmticks=5, wMticks=2, wmticks=1, nyMticks=11, mymticks=4, grid=True,
           grid_ls='solid', grid_col='lightgrey', grid_alpha=0.7, xlog=True):
    # Number of curves
    nc = len(Ly)
    # Checking the parameters
    if ymin is None:
        ymin = np.min(np.asarray([arr.min() for arr in Ly]))
    if ymax is None:
        ymax = np.max(np.asarray([arr.max() for arr in Ly]))
    if Lmarkers is None:
        Lmarkers = ['x'] * nc
    if Lcols is None:
        Lcols = ['blue'] * nc
    if Lleg is None:
        Lleg = [None] * nc
        add_leg = False
    else:
        add_leg = True
    if Lls is None:
        Lls = ['solid'] * nc
    if Lmedw is None:
        Lmedw = [float(lw) / 2.0] * nc
    if Lsdots is None:
        Lsdots = [12] * nc

    # Setting the limits of the y-axis
    y_lim = [ymin, ymax]

    # Defining the ticks on the y-axis
    yMticks = np.linspace(start=ymin, stop=ymax, num=nyMticks, endpoint=True, retstep=False)
    ymticks = np.linspace(start=ymin, stop=ymax, num=1 + mymticks * (nyMticks - 1), endpoint=True, retstep=False)
    yMticksLab = [int(round(v * 100.0)) / 100.0 for v in yMticks]

    # Initial values for xmin and xmax
    xmin, xmax = 1, -np.inf

    fig = plt.figure(figsize=(16, 12))
    ax = fig.add_subplot(111)
    if xlog:
        fplot = ax.semilogx
    else:
        fplot = ax.plot

    # Plotting the data
    for id, y in enumerate(Ly):
        x = np.arange(start=1, step=1, stop=y.size + 0.5, dtype=np.int64)
        xmax = max(xmax, x[-1])
        fplot(x, y, label=Lleg[id], alpha=alpha_plot, color=Lcols[id], linestyle=Lls[id], lw=lw, marker=Lmarkers[id],
              markeredgecolor=Lcols[id], markeredgewidth=Lmedw[id], markersize=Lsdots[id], dash_capstyle='round',
              solid_capstyle='round', dash_joinstyle='round', solid_joinstyle='round', markerfacecolor=Lcols[id],
              markevery=markevery)

    # Setting the limits of the axes
    ax.set_xlim([xmin, xmax])
    ax.set_ylim(y_lim)

    # Setting the major and minor ticks on the y-axis
    ax.set_yticks(yMticks, minor=False)
    ax.set_yticks(ymticks, minor=True)
    ax.set_yticklabels(yMticksLab, minor=False, fontsize=sax)

    # Defining the legend
    if add_leg:
        leg = ax.legend(loc=loc_leg, fontsize=sleg, markerfirst=True, fancybox=True, framealpha=alpha_leg,
                        ncol=ncol_leg)
        if zleg is not None:
            leg.set_zorder(zleg)

    # Setting the size of the ticks labels on the x axis
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(sax)

    # Setting ticks length and width
    ax.tick_params(axis='both', length=lMticks, width=wMticks, which='major')
    ax.tick_params(axis='both', length=lmticks, width=wmticks, which='minor')

    # Setting the positions of the labels
    ax.xaxis.set_tick_params(labelright=False, labelleft=True)
    ax.yaxis.set_tick_params(labelright=False, labelleft=True)

    # Adding the grids
    if grid:
        ax.xaxis.grid(True, linestyle=grid_ls, which='major', color=grid_col, alpha=grid_alpha)
        ax.yaxis.grid(True, linestyle=grid_ls, which='major', color=grid_col, alpha=grid_alpha)
    ax.set_axisbelow(True)

    ax.set_title(tit, fontsize=stit)
    ax.set_xlabel(xlabel, fontsize=sax)
    ax.set_ylabel(ylabel, fontsize=sax)
    # plt.tight_layout()

    # Showing the figure
    plt.show()
    plt.close()


if __name__ == "__main__":
    perplexity = [40, 80, 100]
    L_rnx, L_kg = [], []
    Lmarkers = ['x', 'o', 's', 'v', 'D', "*"]
    Lcols = ['green', 'red', 'blue', 'saddlebrown', 'fuchsia', 'darkcyan']
    Lleg_rnx, Lleg_kg = [], []
    Lls = []
    Lmedw = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    Lsdots = [12, 12, 12, 12, 12, 12]

    logger_config()

    # read dataset
    dataset = read_dataset("datasets/{}.csv".format(dataset_file_name))

    # normalize input
    dataset = normalize_input(dataset)

    # split and reindex the dataset
    dataset_train, dataset_test = train_test_split(dataset, test_size=0.15)
    dataset_train_eval, _ = train_test_split(dataset_train, test_size=0.15)

    dataset_train = dataset_train.reset_index(drop=True)

    dataset_test = dataset_test.reset_index(drop=True)
    dataset_test_labels = dataset_test['label']
    dataset_test = dataset_test.drop(labels=['label'], axis=1)

    dataset_train_eval = dataset_train_eval.reset_index(drop=True)
    dataset_train_eval_labels = dataset_train_eval['label']
    dataset_train_eval = dataset_train_eval.drop(labels=['label'], axis=1)

    # create centroid network
    CN = CentroidNetwork(global_centroids_count=global_centroids, num_processes=2)

    # append layers with M centroids to the network
    CN.append_layer(M=local_centroids)
    # CN.append_layer(M=local_centroids_2)

    # train centroid network
    CN.multiprocess_train(X=dataset_train)

    # feed test data through network
    after_network_test = CN.multiprocess_feed_forward(X=dataset_test)

    score = evaluate(
        real_labels=dataset_test_labels, predicted_labels=after_network_test["label"]
    )
    logging.info("Accuracy for test dataset: {}".format(score))

    d_hd = squareform(X=pdist(X=dataset_test, metric='euclidean'), force='tomatrix')

    for perp in perplexity:
        X_lds = dataset_test

        tsne = TSNE(n_components=2, verbose=1, perplexity=perp, n_iter=500)
        X_lds = tsne.fit_transform(X_lds.values)

        d_ld = squareform(X=pdist(X=X_lds, metric='euclidean'), force='tomatrix')

        print("Computing the DR quality")
        rnxk, auc_rnx = eval_dr_quality(d_hd=d_hd, d_ld=d_ld)

        print("Computing the KNN gain")
        kg, auc_kg = knngain(d_hd=d_hd, d_ld=d_ld, labels=after_network_test['label'].to_numpy())

        L_rnx.append(rnxk)
        L_kg.append(kg)
        Lleg_rnx.append("{a} t-SNE ($perp={perp}$)".format(a=int(round(auc_rnx * 1000)) / 1000.0, perp=perp))
        Lleg_kg.append("{a} t-SNE ($perp={perp}$)".format(a=int(round(auc_kg * 1000)) / 1000.0, perp=perp))
        Lls.append('solid')

    for perp in perplexity:
        X_lds = after_network_test.drop(labels=['label'], axis=1)

        tsne = TSNE(n_components=2, verbose=1, perplexity=perp, n_iter=500)
        X_lds = tsne.fit_transform(X_lds.values)

        d_ld = squareform(X=pdist(X=X_lds, metric='euclidean'), force='tomatrix')

        print("Computing the DR quality")
        rnxk, auc_rnx = eval_dr_quality(d_hd=d_hd, d_ld=d_ld)

        print("Computing the KNN gain")
        kg, auc_kg = knngain(d_hd=d_hd, d_ld=d_ld, labels=after_network_test['label'].to_numpy())

        L_rnx.append(rnxk)
        L_kg.append(kg)
        Lleg_rnx.append("{a} 100-local / 100-global ($perp={perp}$)".format(a=int(round(auc_rnx * 1000)) / 1000.0, perp=perp))
        Lleg_kg.append("{a} 100-local / 100-global ($perp={perp}$)".format(a=int(round(auc_kg * 1000)) / 1000.0, perp=perp))
        Lls.append('solid')

    # Displaying the DR quality criteria
    viz_qa(Ly=L_rnx, Lmarkers=Lmarkers, Lcols=Lcols, Lleg=Lleg_rnx, Lls=Lls, Lmedw=Lmedw, Lsdots=Lsdots,
           tit='DR quality', xlabel='Neighborhood size $K$', ylabel='$R_{NX}(K)$')

    viz_qa(Ly=L_kg, Lmarkers=Lmarkers, Lcols=Lcols, Lleg=Lleg_kg, Lls=Lls, Lmedw=Lmedw, Lsdots=Lsdots, tit='KNN gain',
           xlabel='Neighborhood size $K$', ylabel='$G_{NN}(K)$')

    print("Finished.")
