#!/bin/bash

# shellcheck disable=SC2164
cd "$(dirname "$0")"

DIR="/env/bin/"
if [ -d "$DIR" ]; then
  # Take action if $DIR exists. #
  echo "Virtualenv already exists."
else
  echo "Creating virtualenv in project folder..."
  virtualenv env
fi

source env/bin/activate

echo "Installing python modules in ${DIR}..."
pip install jupyter sklearn scipy numpy pandas matplotlib seaborn multimethod tqdm p_tqdm PyQt5 keras pytest-black pytest

deactivate

