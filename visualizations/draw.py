import matplotlib.pyplot as plt
import numpy as np
import sys

x, y, labels = np.loadtxt(sys.argv[1], delimiter=' ', unpack=True)

unique = list(set(labels))
colors = [plt.cm.jet(float(i) / max(unique)) for i in unique]

plt.figure(figsize=(12.44, 9.77))

for i, u in enumerate(unique):
    xi = [x[j] for j in range(len(x)) if labels[j] == u]
    yi = [y[j] for j in range(len(x)) if labels[j] == u]
    plt.scatter(xi, yi, c=colors[i], label=str(u), s=2)
plt.legend()


plt.savefig(sys.argv[2])
