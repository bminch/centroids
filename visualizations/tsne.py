#!/usr/bin/env python3

import time
import os

import matplotlib.pyplot as plt
import seaborn as sns

from typing import Any, Dict, List
from collections import defaultdict
from sklearn.manifold import TSNE

import pandas as pd

def draw(dataset: Dict) -> None:
    plt.figure(figsize=(16, 10))

    sns.scatterplot(
        x="tsne-2d-one",
        y="tsne-2d-two",
        hue="label",
        palette=sns.color_palette("hls", 10),
        data=dataset,
        legend="full",
        alpha=0.3,
    )

    plt.show()


def save_to_file(
    file_name: str, x: List[float], y: List[float], labels: List[Any]
) -> None:
    with open("../tsne_results/" + file_name + ".txt", "w") as in_file:
        for i in range(0, len(labels)):
            in_file.write(str(x[i]) + " " + str(y[i]) + " " + str(labels[i]) + "\n")


def tsne_experiment(file_name: str) -> None:
    data = pd.read_csv("../output/" + file_name + ".csv", skiprows=2)

    X = data.iloc[:, :-1].values
    Y = data.iloc[:, -1:].values.ravel()

    time_start = time.time()
    tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=500)
    tsne_results = tsne.fit_transform(X)

    print("t-SNE done! Time elapsed: {} seconds".format(time.time() - time_start))

    df = defaultdict()
    df["tsne-2d-one"] = tsne_results[:, 0]
    df["tsne-2d-two"] = tsne_results[:, 1]
    df["label"] = Y

    save_to_file(file_name, df["tsne-2d-one"], df["tsne-2d-two"], df["label"])

    os.system(
        "python ./draw.py ../tsne_results/{}.txt ../tsne_results/{}.png".format(
            file_name, file_name
        )
    )


if __name__ == "__main__":
    dataset_file = 'fmnist_pca100_100_100'
    for variant in ["all"]:
        tsne_experiment(
            "{}_{}".format(
                dataset_file,
                variant
            )
        )

    # dataset_files = ["mnist_70k_pca30"]
    # # dataset_files = ["mnist_7k_pca30"]
    #
    # local_centroids_values = [20, 50]
    # global_centroids_values = [0, 50, 100, 200]
    #
    # if not os.path.exists("../tsne_results/"):
    #     os.makedirs("../tsne_results/")
    #
    # for dataset_file in dataset_files:
    #     for local_centroid in local_centroids_values:
    #         for global_centroid in global_centroids_values:
    #             for variant in ["test", "train", "all"]:
    #                 tsne_experiment(
    #                     "{}_{}_{}_{}".format(
    #                         dataset_file,
    #                         global_centroid,
    #                         local_centroid,
    #                         variant
    #                     )
    #                 )
