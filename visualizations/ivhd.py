import subprocess
import os


def viskit_experiment(file_name: str, iterations: int, nn: int, rn: int = 1) -> None:
    args = ("../viskit_offline", "../output/{}.csv".format(file_name), str(iterations), str(nn), str(rn))
    output = subprocess.check_output(args)
    with open("../ivhd_results/{}.txt".format(file_name), 'wb') as out_file:
        out_file.write(output)

    os.system("python ./draw.py ./visualization.txt ../ivhd_results/{}.png".format(file_name))


if __name__ == "__main__":
    files = ['']
    m_values = ['']
    # m_values = ['5', '10', '20', '50', '100', '200']

    if not os.path.exists('../ivhd_results/'):
        os.makedirs('../ivhd_results/')

    for file in files:
        for m in m_values:
            viskit_experiment("{}_{}".format(file, m), 10000, 2, 1)
